import urllib.request
import urllib.parse

# Don't forget to .encode/.decode('utf-8') for urllib

def application(environ, start_response):
	status = '200 OK'
	response_headers = [('Content-type', 'text/html')]
	start_response(status, response_headers)
	postDataLen = int(environ.get('CONTENT_LENGTH', '0'))
	postData = str(environ['wsgi.input'].read(postDataLen))

	if len(postData) > 3:
		clientParams = urllib.parse.parse_qsl(postData)
		clientSID = clientParams[0][1]
		clientSKey = clientParams[1][1][:-1] #[:-1] removes the last char '\''
		#print("{:s} : {:s}".format(clientSID, clientSKey))
	else:
		print('LoginSessionPoll Failed')
		return 'nope'

	postParams = urllib.parse.urlencode({'s_id': clientSID, 's_key': clientSKey}).encode('utf-8')
	legitPostReply = urllib.request.urlopen("https://api.playwarz.com/WarZ/api/api_LoginSessionPoller.aspx", postParams)
	legitSessionStatus = legitPostReply.read().decode('utf-8')

	#return legitSessionStatus
	return 'WO_0' # Always return session valid? ( It changes to W0_1 when your session expires, or when fairfight kicks you :V )
