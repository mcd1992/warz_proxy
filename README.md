warz_proxy
==========

A proxy for the warz login and content servers.

This is a set of wsgi scripts that I ran to proxy the WarZ login and shop servers. Since they were going up and down so much this was helpful to login and bypass the inventory checks to play. Once you were in you would have your old inventory.
