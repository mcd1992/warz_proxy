import urllib.request
import sqlite3

INVALID_TOKEN = 'WO_00 0 0'

#def saveUserCreds(username, password, token):
	# fuck json, lets sqlite

# Don't forget to .encode/.decode('utf-8') for urllib
def application(environ, start_response):
	status = '200 OK'
	response_headers = [('Content-type', 'text/html')]
	start_response(status, response_headers)
	postDataLen = int(environ.get('CONTENT_LENGTH', '0'))
	postData = str(environ['wsgi.input'].read(postDataLen))

	if len(postData) > 3:
		clientParams = urllib.parse.parse_qsl(postData)
		clientUsername = clientParams[0][1]
		clientPassword = clientParams[1][1][:-1] #[:-1] removes the last char '\''
	else:
		print('api_Login failed')
		return 'nope'

	postParams = urllib.parse.urlencode({'username': clientUsername, 'password': clientPassword}).encode('utf-8')
	legitPostReply = urllib.request.urlopen("https://api.playwarz.com/WarZ/api/api_Login.aspx", postParams)
	legitLoginToken = legitPostReply.read().decode('utf-8')

	#saveUserCreds(clientUsername, clientPassword, legitLoginToken)

	return legitLoginToken
	#return INVALID_TOKEN
