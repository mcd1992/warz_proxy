import urllib.request
import urllib.parse
import os

def cacheFile(sid, name, data):
	if not os.path.exists('/srv/www/warz/playerCache/'+sid):
		os.makedirs('/srv/www/warz/playerCache/'+sid)
	hand = open('/srv/www/warz/playerCache/'+sid+'/'+name, 'wb')
	hand.write(data)
	hand.close()
	return hand.closed

def cacheHandle(sid, name):
	hand = open('/srv/www/warz/playerCache/'+sid+'/'+name)
	return hand

# Don't forget to .encode/.decode('utf-8') for urllib
def application(environ, start_response):
	status = '200 OK'
	response_headers = [('Content-type', 'text/html')]
	start_response(status, response_headers)
	postDataLen = int(environ.get('CONTENT_LENGTH', '0'))
	postData = str(environ['wsgi.input'].read(postDataLen))

	if len(postData) > 3:
		clientParams = urllib.parse.parse_qsl(postData)
		clientSID = clientParams[0][1]
		clientSKey = clientParams[1][1][:-1] #[:-1] removes the last char '\''
		#print("{:s} : {:s}".format(clientSID, clientSKey))
	else:
		print('GetProfile Failed')
		return 'nope'

	postParams = urllib.parse.urlencode({'s_id': clientSID, 's_key': clientSKey}).encode('utf-8')
	legitPostReply = urllib.request.urlopen("https://api.playwarz.com/WarZ/api/api_GetProfile1.aspx", postParams)

	cacheFile(clientSID, 'getprofile', legitPostReply.read())

	if 'wsgi.file_wrapper' in environ:
		return environ['wsgi.file_wrapper'](cacheHandle(clientSID, 'getprofile'), 1024)
	else:
		return iter(lambda: legitPostReply.read(1024), '')
